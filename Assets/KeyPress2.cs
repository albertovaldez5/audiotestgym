﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyPress2 : MonoBehaviour
{
	public AudioClip impact;
	AudioSource audioSource;
	void Start()
	{
		audioSource = GetComponent<AudioSource>();
	}

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.F))
		{
			//print("Before:" + Time.time);
			//print("DSP Before:" + AudioSettings.dspTime);
			double startTime = Time.realtimeSinceStartup;
			audioSource.PlayOneShot(impact, 1.0f);
			print($"UNITY FX TIME: {((Time.realtimeSinceStartup - startTime) * 1000.0).ToString("F6")} MILLISECONDS");
			//print("After:" + Time.time);
			//print("DSP After:" + AudioSettings.dspTime);
		}
	}
}
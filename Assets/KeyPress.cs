﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyPress : MonoBehaviour
{

	// Update is called once per frame
	void Update()
	{

		if (Input.GetKeyDown("d"))
		{

			//print("Playing FMOD Before:" + Time.time);
			//print("Playing FMOD DSP Before:" + AudioSettings.dspTime);
			double startTime = Time.realtimeSinceStartup;
			FMODUnity.RuntimeManager.PlayOneShot("event:/shot");
			print($"FMOD FX TIME: {((Time.realtimeSinceStartup - startTime) * 1000.0).ToString("F6")} MILLISECONDS");
			//print("Playing FMOD After:" + Time.time);
			//print("Playing FMOD After:" + AudioSettings.dspTime);
		}
	}
}
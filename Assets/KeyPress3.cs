﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyPress3 : MonoBehaviour
{

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown("a"))
        {
            print("Before FMOD:" + Time.time);
            print("DSP Before FMOD:" + AudioSettings.dspTime);
            FMODUnity.RuntimeManager.PlayOneShot("event:/evento1");
            print("After FMOD:" + Time.time);
            print("DSP After FMOD:" + AudioSettings.dspTime);
        }
    }
}

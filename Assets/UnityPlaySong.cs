﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnityPlaySong : MonoBehaviour
{
	public AudioClip song;
	private double startTime;
	private AudioSource audioSource;
	private bool isFlag = false;

	private void Start()
	{
		startTime = Time.realtimeSinceStartup;
		audioSource = GetComponent<AudioSource>();

		// PLAY MUSIC HERE !!!!
		audioSource.PlayOneShot(song, 0.25f);
	}

	// Update is called once per frame
	void Update()
	{
		if (!audioSource.isPlaying && isFlag == false)
		{
			print($"UNITY SONG TIME: {(Time.realtimeSinceStartup - startTime).ToString("F6")} SECONDS");
			isFlag = true;
		}
	}
}